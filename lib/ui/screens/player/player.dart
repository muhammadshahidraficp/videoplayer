import 'dart:io';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_dialogs/flutter_dialogs.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:permission_handler/permission_handler.dart';


class Player extends StatefulWidget {
  final TargetPlatform? platform;

  Player({Key? key, this.platform}) : super(key: key);

  @override
  _PlayerState createState() => _PlayerState();
}

class _PlayerState extends State<Player> {
  late VideoPlayerController _videoPlayerController;
  ChewieController? _chewieController;
  var selectedIndex = 0;
  late String _localPath;
  late bool _permissionReady;

  List urlList = [
    'https://assets.mixkit.co/videos/preview/mixkit-a-girl-blowing-a-bubble-gum-at-an-amusement-park-1226-large.mp4',
    'https://assets.mixkit.co/videos/preview/mixkit-daytime-city-traffic-aerial-view-56-large.mp4',
    'https://assets.mixkit.co/videos/preview/mixkit-a-girl-blowing-a-bubble-gum-at-an-amusement-park-1226-large.mp4',
    'https://assets.mixkit.co/videos/preview/mixkit-daytime-city-traffic-aerial-view-56-large.mp4',
  ];

  @override
  void initState() {
    super.initState();
    _permissionReady = false;
    _retryRequestPermission();
    initializePlayer(0);
    Future.delayed(Duration.zero,() {
    checkInternetConnection(context);
    });
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController?.dispose();
    super.dispose();
  }

  Future<void> initializePlayer(index) async {
    try{
    _videoPlayerController = VideoPlayerController.network(urlList[index]);
    await Future.wait([_videoPlayerController.initialize()]);
    _createChewieController();
    }catch(e){

    }
  }

  checkInternetConnection(context)async{
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
      }
    } on SocketException catch (_) {
        _showAlert(context);
        print('not connected');
    }
  }

  void _createChewieController() {
    _chewieController = ChewieController(
        aspectRatio: 16 / 9,
        showControls: true,
        showControlsOnInitialize: true,
        videoPlayerController: _videoPlayerController,
        autoPlay: true,
        looping: true);
  }

  changeVideo(index) {
    initializePlayer(index);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double width = size.width;
    return body(width);
  }

  _showAlert(BuildContext context) {
    showPlatformDialog(
      context: context,
      builder: (_) => BasicDialogAlert(
        title: Text("No Internet"),
        content:
            Text("Please check your internet connection"),
        actions: <Widget>[
          BasicDialogAction(
            title: Text("OK"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }

  Container bottomSlideMenu() {
    return Container(
      color: Colors.grey.shade100,
      child: Row(
        children: [
          GestureDetector(
            onTap: () {
              if(selectedIndex>0){
              setState(() {
                selectedIndex -= 1;
              });
              changeVideo(selectedIndex);
              }
            },
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: Colors.white,
                ),
                margin: EdgeInsets.only(left: 10),
                child: Icon(Icons.chevron_left),
              ),
          ),
          Spacer(),
          ElevatedButton.icon(
            onPressed: () {
              downloadTask();
            },
            label: Text(
              'Download',
              style: TextStyle(color: Colors.black),
            ),
            icon: Icon(
              Icons.file_download,
              color: Colors.green,
            ),
            style: ElevatedButton.styleFrom(
              shape: StadiumBorder(),
              primary: Colors.white,
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              if(selectedIndex<2){
                setState(() {
                  selectedIndex += 1;
                });
                changeVideo(selectedIndex);
              }
            },
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5),)
              ),
              margin: EdgeInsets.only(right: 10),
              child: Icon(Icons.chevron_right),
            ),
          )
        ],
      ),
    );
  }

  Scaffold body(width) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                _chewieController != null &&
                        _chewieController!
                            .videoPlayerController.value.isInitialized
                    ? Container(
                        alignment: Alignment.center,
                        width: width,
                        height: 200,
                        child: Chewie(
                          controller: _chewieController!,
                        ),
                      )
                    : Container(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.menu_outlined,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                      iconSize: 40,
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 10),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                        child: Container(
                          height: 40,
                          width: 40,
                          color: Colors.grey.shade100,
                          child: Center(
                            child: Image.asset('images/avatar.jpg'),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
            bottomSlideMenu()
          ],
        ),
      ),
    );
  }

  Future<String?> _findLocalPath() async {
    final directory = widget.platform == TargetPlatform.android
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    return directory?.path;
  }

  Future<void> _prepareSaveDir() async {
    _localPath =
        (await _findLocalPath())! + Platform.pathSeparator + 'Download';

    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Widget _buildNoPermissionWarning() => Container(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: Text(
                  'Please grant accessing storage permission to continue -_-',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.blueGrey, fontSize: 18.0),
                ),
              ),
              SizedBox(
                height: 32.0,
              ),
              FlatButton(
                  onPressed: () {
                    _retryRequestPermission();
                  },
                  child: Text(
                    'Retry',
                    style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0),
                  ))
            ],
          ),
        ),
      );

  Future<bool> _checkPermission() async {
    if (widget.platform == TargetPlatform.android) {
      final status = await Permission.storage.status;
      if (status != PermissionStatus.granted) {
        final result = await Permission.storage.request();
        if (result == PermissionStatus.granted) {
          return true;
        }
      } else {
        return true;
      }
    } else {
      return true;
    }
    return false;
  }

  Future<void> _retryRequestPermission() async {
    final hasGranted = await _checkPermission();

    if (hasGranted) {
      await _prepareSaveDir();
    }

    setState(() {
      _permissionReady = hasGranted;
    });
  }

  downloadTask() async {
    await FlutterDownloader.enqueue(
        url: urlList[selectedIndex],
        savedDir: _localPath,
        showNotification: true,
        openFileFromNotification: true);
  }
}
